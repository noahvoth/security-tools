import platform
import psutil
import socket
from tabulate import tabulate

def get_host_os():
    return platform.system()

def get_running_services():
    if get_host_os() == 'Darwin':
        return [["Unsupported OS. Only macOS (Darwin) is supported."]]
    else:
        return [["Unsupported OS. Only macOS (Darwin) is supported."]]

def get_open_ports():
    open_ports = []
    if get_host_os() == 'Darwin':
        for conn in psutil.net_connections(kind='tcp'):
            if conn.status == 'LISTEN':
                port = conn.laddr.port
                try:
                    service_name = socket.getservbyport(port)
                except OSError:
                    service_name = "Unknown"
                open_ports.append([port, service_name])
    else:
        return [["Unsupported OS. Only macOS (Darwin) is supported."]]
    return open_ports

def get_service_version(service_name):
    # Implement logic to retrieve version information of services
    # based on the service_name
    return "Version information not found."

# Retrieve the information
host_os = [["Host OS", get_host_os()]]
running_services = get_running_services()
open_ports = get_open_ports()
service_versions = []
for port_info in open_ports:
    port = port_info[0]
    service = port_info[1]
    version = get_service_version(service)
    service_versions.append([port, service, version])

# Tabulate the results
table_data = host_os + running_services + service_versions
headers = ["ID", "Service", "Port", "Service Name", "Version"]
table = tabulate(table_data, headers, tablefmt="grid")

# Display the table
print(table)

