import platform
import psutil
import socket
from tabulate import tabulate

# Retrieve system information
system_information = {
    "Hostname": platform.node(),
    "Operating System": f"{platform.system()} {platform.release()}",
    "Processor": platform.processor(),
    "Memory": f"{psutil.virtual_memory().total / (1024 ** 3):.2f} GB",
    "Storage": f"{psutil.disk_usage('/').total / (1024 ** 3):.2f} GB",
    "Uptime": f"{psutil.boot_time():.2f}",
}

# Retrieve network information
network_interfaces = psutil.net_if_addrs()
network_information = []
for interface, addresses in network_interfaces.items():
    for address in addresses:
        if address.family == socket.AF_INET:
            network_information.append([interface, address.address, 
address.netmask, address.broadcast])

# Tabulate the system information
system_table = tabulate(system_information.items(), headers=["Category", 
"Value"], tablefmt="grid")

# Tabulate the network information
network_table = tabulate(network_information, headers=["Interface", "IP Address", "Netmask", "Broadcast"], tablefmt="grid")

# Print the tabulated output
print("System Information:")
print(system_table)
print("\nNetwork Information:")
print(network_table)
