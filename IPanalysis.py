import string
import csv
from collections import defaultdict
from datetime import datetime
from tabulate import tabulate

# Read the CSV file
data = []
with open('logs.csv', 'r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        data.append(row)

# Convert the 'time' column to datetime format
for row in data:
    time_str = row['time'].replace(' UTC', '')  # Remove ' UTC' from the string
    row['time'] = datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S')

# Calculate the frequency of each unique IP address for each actor_id
ip_frequency = defaultdict(lambda: defaultdict(list))
for row in data:
    actor_id = row['actor_id']
    ip = row['ip']
    ip_frequency[actor_id][ip].append(row['time'])

# Prepare the tabulated result
result = []
for actor_id, frequencies in ip_frequency.items():
    change_count = sum(1 for ip_times in frequencies.values() if len(ip_times) > 1)
    if change_count > 3:
        for ip, ip_times in frequencies.items():
            if len(ip_times) > 1:
                frequency = len(ip_times) - 1  # Number of times the IP has changed
                result.append([actor_id, ip, frequency])

# Create the ASCII art Banner
banner = r"""

_____   ______  ______ _____   |  |  |__|________  ____ _______ 
\__  \  \____ \ \____ \\__  \  |  |  |  |\___   /_/ __ \\_  __ \
 / __ \_|  |_> >|  |_> >/ __ \_|  |__|  | /    / \  ___/ |  | \/
(____  /|   __/ |   __/(____  /|____/|__|/_____ \ \___  >|__|   
     \/ |__|    |__|        \/                 \/     \/        
"""

# Display the tabulated result
headers = ["Actor ID", "Potential Risks", "Frequency"]
print(f"{banner}")
print(tabulate(result, headers=headers))

