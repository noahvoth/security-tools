# Security Tools

Tools used to perform security tasks and responses

## Getting started

Scanner2.py - Network basic information return
InfoHost.py - Host information
IPAnalizer.py - Applizer. - this takes the csv from same directory of a security log for logins/out and compares against the Agen_ID and IP address.  If there is a change in the IP Address greater than 4, then print the IP addresses accsociated with the Agent ID and how frequant they have been seen in a tablulated manner.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/noahvoth/security-tools.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] These are not to be added to any additional project outside of this repo.


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
Security Tools


